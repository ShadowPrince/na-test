//
//  NAZKApi.h
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NAZKObjects.h"

typedef void (^NAZKApiQueryCallback)(NAZKQueryResult *, NSError *);

@interface NAZKApi : NSObject
+ (NAZKApi *) shared;
- (void) query:(NSString *) searchString callback:(NAZKApiQueryCallback) rawCb;

@end
