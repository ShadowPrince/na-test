//
//  NATRootViewController.m
//  na-test
//
//  Created by shdwprince on 4/30/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "NATRootViewController.h"

@interface NATRootViewController ()
@property NATItemTableViewController *searchTableViewController;
@property NATItemTableViewController *favoriteTableViewController;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *showFavsButton;

@end @implementation NATRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.favoriteTableViewController = [[NATItemTableViewController alloc] initWithNibName:@"NATItemTableViewController" bundle:nil];
    self.favoriteTableViewController.modalPresentationStyle = UIModalPresentationPopover;
    self.favoriteTableViewController.preferredContentSize = CGSizeMake(300, 300);
    self.favoriteTableViewController.popoverPresentationController.delegate = self;
    self.favoriteTableViewController.popoverPresentationController.sourceView = self.showFavsButton;
    self.favoriteTableViewController.type = NATItemTableViewControllerTypeFavorites;
    
    [self updateShowFavsButton];
}

- (void) updateShowFavsButton {
    [self.showFavsButton setTitle:[NSString stringWithFormat:NATRootViewControllerFavButtonTitle, [NATFavorites shared].items.count] forState:UIControlStateNormal];
}

#pragma mark - actions
- (IBAction) showPdfAction:(NAZKItem *) item {
    [self performSegueWithIdentifier:NATRootViewControllerShowPdfSegueIdentifier sender:item.pdfUrl];
}

- (IBAction) favoriteAction:(NAZKItem *) item {
    [self updateShowFavsButton];
}

- (IBAction)showFavsAction:(id)sender {
    [self.favoriteTableViewController populateWithContent:[NATFavorites shared].items];

    [self presentViewController:self.favoriteTableViewController animated:YES completion:nil];
}

- (IBAction)searchAction:(id)sender {
    [[NAZKApi shared] query:self.searchTextField.text callback:^(NAZKQueryResult *result, NSError *error) {
        [self.searchTableViewController populateWithContent:result.items];
    }];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [self searchAction:nil];
    return YES;
}

#pragma mark - segues
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:NATRootViewControllerShowPdfSegueIdentifier]) {
        NATPDFViewController *dest = (NATPDFViewController *) segue.destinationViewController;
        dest.pdfUrl = (NSURL *) sender;
    } else if ([segue.identifier isEqualToString:NATRootViewControllerSearchTableEmbedSegueIdentifier]) {
        self.searchTableViewController = (NATItemTableViewController *) segue.destinationViewController;
    }

    [super prepareForSegue:segue sender:sender];
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller traitCollection:(UITraitCollection *)traitCollection {
    return UIModalPresentationNone;
}

@end
