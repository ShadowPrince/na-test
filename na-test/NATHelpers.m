//
//  NATHelpers.m
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "NATHelpers.h"

@implementation NATHelpers

@end


@implementation UIResponder (PassDownTheChain)
- (void) passSelector:(SEL) sel withSender:(id) sender {
    UIResponder *resp = [self nextResponder];
    if ([resp respondsToSelector:sel]) {
        [resp performSelector:sel withObject:sender];
    } else {
        [resp passSelector:sel withSender:sender];
    }
}

@end
