//
//  NAZKObjects.m
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "NAZKObjects.h"

@implementation NAZKItem

- (BOOL) isEqual:(id)object {
    if ([object isKindOfClass:[NAZKItem class]]) {
        return [self.identifier isEqual:[(NAZKItem *) object identifier]];
    } else {
        return [super isEqual:object];
    }
}

@end

@implementation NAZKQueryResult

- (instancetype) init {
    self = [super init];
    self.items = [NSMutableArray new];
    return self;
}

@end
