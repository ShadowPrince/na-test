//
//  NATFavorites.m
//  na-test
//
//  Created by shdwprince on 4/30/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "NATFavorites.h"

@implementation NATFavoriteItem
+ (instancetype) favoriteFromItem:(NAZKItem *) item {
    NATFavoriteItem *instance = [NATFavoriteItem new];
    instance.identifier = item.identifier;
    instance.firstName = item.firstName;
    instance.lastName = item.lastName;
    instance.companyName = item.companyName;
    instance.positionName = item.positionName;
    instance.pdfUrl = item.pdfUrl;

    return instance;
}

@end

@interface NATFavorites()

@end @implementation NATFavorites
static NATFavorites *_shared;

- (instancetype) init {
    self = [super init];
    self.items = [NSMutableArray new];

    return self;
}

- (void) favoriteItem:(NAZKItem *) item {
    [self.items addObject:[NATFavoriteItem favoriteFromItem:item]];
}

- (void) unfavoriteItem:(NAZKItem *) item {
    [self.items removeObject:(NATFavoriteItem *) item];
}

- (void) setComment:(NSString *)str forItem:(NAZKItem *)item {
    NSUInteger idx = [self.items indexOfObject:(NATFavoriteItem *) item];

    self.items[idx].comment = str;
}

- (NSString *) commentForItem:(NAZKItem *)item {
    NSUInteger idx = [self.items indexOfObject:(NATFavoriteItem *) item];

    return self.items[idx].comment;
}

- (BOOL) isFavorite:(NAZKItem *)item {
    return [self.items containsObject:(NATFavoriteItem *) item];
}

+ (instancetype) shared {
    if (!_shared) {
        _shared = [NATFavorites new];
    }

    return _shared;
}

@end
