//
//  NAZKObjects.h
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NAZKItem : NSObject
@property NSString *identifier;
@property NSString *firstName;
@property NSString *lastName;
@property NSString *companyName;
@property NSString *positionName;
@property NSURL *pdfUrl;

@end

@interface NAZKQueryResult : NSObject
@property NSMutableArray<NAZKItem *> *items;

@end
