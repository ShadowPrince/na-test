//
//  NATFavorites.h
//  na-test
//
//  Created by shdwprince on 4/30/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NAZKObjects.h"

@interface NATFavoriteItem : NAZKItem
@property NSString *comment;

+ (instancetype) favoriteFromItem:(NAZKItem *) item;
@end

@interface NATFavorites : NSObject
@property NSMutableArray<NATFavoriteItem *> *items;

- (void) favoriteItem:(NAZKItem *) item;
- (void) unfavoriteItem:(NAZKItem *) item;
- (void) setComment:(NSString *) str forItem:(NAZKItem *) item;
- (NSString *) commentForItem:(NAZKItem *) item;
- (BOOL) isFavorite:(NAZKItem *) item;

+ (instancetype) shared;
@end
