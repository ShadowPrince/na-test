//
//  NATHelpers.h
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NATHelpers : NSObject

@end

@interface UIResponder (PassDownTheChain)
- (void) passSelector:(SEL) sel withSender:(id) sender;
@end
