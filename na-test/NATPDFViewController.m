//
//  NATPDFViewController.m
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "NATPDFViewController.h"

@interface NATPDFViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end @implementation NATPDFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.pdfUrl]];
}

- (IBAction)closeButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
