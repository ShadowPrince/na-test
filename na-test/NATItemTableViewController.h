//
//  ViewController.h
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NAZKApi.h"

#import "NATItemTableViewCell.h"
#import "NATFavorites.h"

typedef enum : NSUInteger {
    NATItemTableViewControllerTypeNormal,
    NATItemTableViewControllerTypeFavorites
} NATItemTableViewControllerType;

typedef NSArray<NAZKItem *> NATItemTableViewControllerContent;

@interface NATItemTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property NATItemTableViewControllerType type;

- (void) populateWithContent:(NATItemTableViewControllerContent *) content;

@end

