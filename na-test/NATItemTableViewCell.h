//
//  NATItemTableViewCell.h
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NAZKObjects.h"
#import "NATHelpers.h"

typedef enum : NSUInteger {
    NATItemTableViewCellNormal,
    NATItemTableViewCellFavorited,
    NATItemTableViewCellExpanded,
} NATItemTableViewCellFavButtonState;

@interface NATItemTableViewCell : UITableViewCell<UITextViewDelegate>
@property NAZKItem *item;

- (void) populateWithItem:(NAZKItem *) item atPath:(NSIndexPath *) path;
- (void) setFavButtonState:(NATItemTableViewCellFavButtonState) state;
- (void) setFavComment:(NSString *) comment;

- (void) expand;
- (void) shrink;

+ (CGFloat) expandedHeight;
+ (CGFloat) shrinkedHeight;

@end
