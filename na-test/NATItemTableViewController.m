//
//  ViewController.m
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "NATItemTableViewController.h"

@interface NATItemTableViewController ()
@property NSMutableArray<NSIndexPath *> *expandedRows;
@property NSArray<NAZKItem *> *content;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end @implementation NATItemTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.expandedRows = [NSMutableArray new];
    [self.tableView registerNib:[UINib nibWithNibName:@"NATItemTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
}

- (void) populateWithContent:(NATItemTableViewControllerContent *) content {
    self.content = content;
    [self.expandedRows removeAllObjects];

    [self.tableView reloadData];
}

- (IBAction) favoriteAction:(NAZKItem *) item {
    NSIndexPath *path = [self pathFromItem:item];
    BOOL deleted = NO;

    if ([[NATFavorites shared] isFavorite:item]) {
        if ([self.expandedRows containsObject:path]) {
            [[NATFavorites shared] unfavoriteItem:item];
            deleted = YES;
            [self.expandedRows removeObject:path];
        } else {
            [self.expandedRows addObject:path];
        }
    } else {
        [[NATFavorites shared] favoriteItem:item];
        [self.expandedRows addObject:path];
    }

    if (!deleted || self.type == NATItemTableViewControllerTypeNormal) {
        [self.tableView reloadRowsAtIndexPaths:@[path, ] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else if (deleted && self.type == NATItemTableViewControllerTypeFavorites) {
        [self.tableView deleteRowsAtIndexPaths:@[path, ] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    [self passSelector:@selector(favoriteAction:) withSender:nil];
}

- (IBAction) favoriteChangedComment:(NSArray *) notification {
    [[NATFavorites shared] setComment:notification[1] forItem:notification[0]];
}

#pragma mark - table
- (NAZKItem *) itemFromPath:(NSIndexPath *) path {
    return self.content[path.row];
}

- (NSIndexPath *) pathFromItem:(NAZKItem *) item {
    return [NSIndexPath indexPathForRow:[self.content indexOfObject:item] inSection:0];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.content.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NAZKItem *item = [self itemFromPath:indexPath];
    NATItemTableViewCell *cell = (NATItemTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"Cell"];

    if ([[NATFavorites shared] isFavorite:item]) {
        [cell setFavComment:[[NATFavorites shared] commentForItem:item]];
        if ([self.expandedRows containsObject:indexPath]) {
            [cell expand];
            [cell setFavButtonState:NATItemTableViewCellExpanded];
        } else {
            [cell shrink];
            [cell setFavButtonState:NATItemTableViewCellFavorited];
        }
    } else {
        [cell setFavButtonState:NATItemTableViewCellNormal];
    }

    [cell populateWithItem:item atPath:indexPath];
    return cell;
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.expandedRows removeAllObjects];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.expandedRows containsObject:indexPath] ? [NATItemTableViewCell expandedHeight] : [NATItemTableViewCell shrinkedHeight];
}

@end
