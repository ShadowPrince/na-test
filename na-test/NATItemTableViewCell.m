//
//  NATItemTableViewCell.m
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "NATItemTableViewCell.h"

@interface NATItemTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UIButton *showPdfButton;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@end @implementation NATItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

#pragma mark - population
- (void) populateWithItem:(NAZKItem *) item atPath:(NSIndexPath *) path {
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@", item.firstName, item.lastName];
    self.companyLabel.text = item.companyName;
    self.positionLabel.text = item.positionName ? item.positionName : @"No position";

    self.showPdfButton.enabled = item.pdfUrl != nil;

    self.item = item;
}

- (void) setFavButtonState:(NATItemTableViewCellFavButtonState) state {
    switch (state) {
        case NATItemTableViewCellNormal:
            [self.favoriteButton setTitle:@"Favorite" forState:UIControlStateNormal];
            break;
        case NATItemTableViewCellExpanded:
            [self.favoriteButton setTitle:@"Remove" forState:UIControlStateNormal];
            break;
        case NATItemTableViewCellFavorited:
            [self.favoriteButton setTitle:@"Expand" forState:UIControlStateNormal];
            break;
    }
}

- (void) setFavComment:(NSString *) comment {
    self.commentTextView.text = comment;
}

#pragma mark - heights
- (void) expand {
    self.commentTextView.hidden = NO;
    self.commentLabel.hidden = NO;

    CGRect frame = self.frame;
    frame.size.height = [NATItemTableViewCell expandedHeight];
    self.frame = frame;
}

- (void) shrink {
    self.commentTextView.hidden = YES;
    self.commentLabel.hidden = YES;

    CGRect frame = self.frame;
    frame.size.height = [NATItemTableViewCell shrinkedHeight];
    self.frame = frame;
}

+ (CGFloat) expandedHeight {
    return [self shrinkedHeight] + 83.f;
}

+ (CGFloat) shrinkedHeight {
    return 74.f;
}

#pragma mark - actions
- (IBAction)pdfButtonAction:(id)sender {
    [self passSelector:@selector(showPdfAction:) withSender:self.item];
}

- (IBAction)favoriteButtonAction:(id)sender {
    [self passSelector:@selector(favoriteAction:) withSender:self.item];
}

- (void) textViewDidChange:(UITextView *)textView {
    [self passSelector:@selector(favoriteChangedComment:) withSender:@[self.item, textView.text, ]];
}

@end
