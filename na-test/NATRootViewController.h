//
//  NATRootViewController.h
//  na-test
//
//  Created by shdwprince on 4/30/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NAZKApi.h"

#import "NATFavorites.h"
#import "NATPDFViewController.h"
#import "NATItemTableViewController.h"

static NSString *NATRootViewControllerShowPdfSegueIdentifier = @"showPdfSegue";
static NSString *NATRootViewControllerSearchTableEmbedSegueIdentifier = @"searchTableEmbedSegue";
static NSString *NATRootViewControllerFavTablePopSegueIdentifier = @"favsTablePopSegue";

static NSString *NATRootViewControllerFavButtonTitle = @"Favorites (%d)";

@interface NATRootViewController : UIViewController<UIPopoverPresentationControllerDelegate>

@end
