//
//  NAZKApi.m
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import "NAZKApi.h"

static NSString *NAZKApEmptyFieldValue = @"ні";

@interface NAZKApi()
@property NSURL *apiUrl;

@end @implementation NAZKApi

- (instancetype) init {
    self = [super init];
    self.apiUrl = [NSURL URLWithString:@"https://public-api.nazk.gov.ua/v1/"];
    return self;
}

static NAZKApi *_shared;
+ (NAZKApi *) shared {
    if (_shared == nil) {
        _shared = [NAZKApi new];
    }

    return _shared;
}

- (void) query:(NSString *) searchString callback:(NAZKApiQueryCallback) rawCb {
    void (^cb)(NAZKQueryResult *, NSError *)= ^void(NAZKQueryResult *result, NSError *error) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            rawCb(result, error);
        }];
    };

    NSString *args = [NSString stringWithFormat:@"?q=%@", [searchString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
    NSURL *url = [NSURL URLWithString:[@"declaration/" stringByAppendingString:args] relativeToURL:self.apiUrl];
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!data) {
            cb(nil, error);
        }

        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        if (!dict) {
            cb(nil, error);
        }

        NAZKQueryResult *result = [NAZKQueryResult new];
        for (NSDictionary *itemDict in dict[@"items"]) {
            NAZKItem *itemObject = [NAZKItem new];
            itemObject.identifier = itemDict[@"id"];
            itemObject.firstName = itemDict[@"firstname"];
            itemObject.lastName = itemDict[@"lastname"];
            itemObject.pdfUrl = [NSURL URLWithString:itemDict[@"linkPDF"]];

            itemObject.companyName = itemDict[@"placeOfWork"];

            NSString *positionFromDict = itemDict[@"position"];
            itemObject.positionName = [positionFromDict isEqualToString:NAZKApEmptyFieldValue] ? nil : positionFromDict;

            [result.items addObject:itemObject];
        }

        cb(result, nil);
    }] resume];
}

@end
