//
//  NATPDFViewController.h
//  na-test
//
//  Created by shdwprince on 4/29/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NATPDFViewController : UIViewController
@property NSURL *pdfUrl;

@end
